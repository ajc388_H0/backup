package backup;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWriteMode;
import com.dropbox.core.DbxException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;
import static java.lang.System.out;
import static java.lang.System.exit;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

//Research LZ4 - GZIP compresses faster

/**
 * A program that compresses list of directories or files and 
 * moves them to a remote location. Currently, only one remote location
 * is supported, that being drop box.
 * 
 * @author Andrew Carter
 * @version 0.2
 * @since 2015-02/22
 * @
 */
public class Backup 
{
    //Process variables
    private static final int backupTimeInterval =  3600000; //hourly time interval
    private static final String relatedScreenName = "MC_" + Backup.worldName;
    private static final int expectedScreenOutputs = 3; //It is always one more than expected, I run this in two screens.
    
    //Backup variables
    private static final String worldName = "HubWorld";
    private static final File backupDirSrc = new File("/home/mc/mcf_server/" + worldName); //Directory that needs to be backed up
    private static final File backupDirDest = new File("/home/mc/mcf_server/backups"); //Directory data should be temporarily stored
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static String backupFileName;
    private static String zipDest;
    private static ZipOutputStream zipOutStream;
    private static FileInputStream inStream;
    
    
    //Dropbox Minecraft_Backup
    private static final String Access_Token = "YFsyUME30RwAAAAAAAACjE0E4avpT5tS0Z5YkUdPhVUydHGOvOlyL3Qqtw4cDjrR";
    
    //Cleanup variabls
    private static final int recentFileStorageThreshold = 12; //ideal count of recent hourly backups 
    private static final int historicalFileStorageThreshold = 14; //ideal count of days of backups
    private static final int localFileStoreageThreshold = 1; //hourly recent backups located on server
    
    /**
     * The main method is responsible for creating the timer thread
     * that compresses and sends files to the drop box location.
    * @param args the command line arguments
    */
    public static void main(String [] args) 
    {
        //Sets up the timer
        Timer timer = new Timer("BackupScript");
        TimerTask backupProcess = new TimerTask() 
        {
            /**
             * The body of the timer thread. This is responsible 
             * for checking if the parent process is running,
             * compressing the files, and writing them to drop box.
             */
            @Override
            public void run() 
            {
                //Related process is running.
                if ( isRelatedProcessRunning() ) 
                {
                    out.println(relatedScreenName + " is running.");
                    
                    compress(); //Compresses the targeted directory
                    copyToDropbox(); //Copies the recently compressed file to dropbox
                    localStorageCleanup(); //Deletes older backups on local storage
                    
                    if ( (new Date()).getHours() == 18 ) 
                        dropboxCleanup(); //Deletes and organizaes backups on dropbox
                }
                else 
                {
                    out.println(relatedScreenName + " screen is not running, program will terminate.");
                    exit(1);                  
                }
            }
        };
        
        timer.schedule(backupProcess, 0, backupTimeInterval);     
    }
    
    /*=================================================================
     ||                      Helper Methods                          ||
    ===================================================================*/
    /**
    * Executes the screen command in the shell to see 
    * if the parent process is running. 
    * @return true if parent process is running
    */
    private static boolean isRelatedProcessRunning() 
    {
        Process proc;
        String line = "";
        int count = 0;
            
        try 
        {
            //Execute shell command to see if screen is still running
            String cmd = "ls /var/run/screen/S-mc | grep " + relatedScreenName;
            out.println("Command: " + cmd);
            out.println("Runtime: " + Runtime.getRuntime().toString());
            proc = Runtime.getRuntime().exec(cmd);
            proc.waitFor();
            
            //Read through the results to make sure only two lines are returned
            //Two lines will be returned due to the nature of the ls command
            BufferedReader commandReader = new BufferedReader( new InputStreamReader(proc.getInputStream()));
            while ((line = commandReader.readLine())!= null) 
            {
                out.println("Output: " + line);
                count++;
            }
            
            out.println("The dependent process is running: " + (count == expectedScreenOutputs) );
            return (count == expectedScreenOutputs); //returns true if dependent process is running
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            return false;
        }
    }
        
    /*=================================================================
     ||                   Compression Methods                        ||
    ===================================================================*/
    /**
    * Creates a ZIP of all of the files/directories 
    */
    private static void compress() 
    {
        //Compression process has been initiated
        backupFileName = worldName + "_" + dateFormat.format(new Date()) + ".zip";
        zipDest = backupDirDest + "/" + backupFileName;
        out.println(zipDest);
        out.println("The file compression process has started at " +  (new Date()).toString() );
        
        //Compress
        try 
        {
            zipOutStream = new ZipOutputStream(new FileOutputStream(zipDest));
            compressDirectory(backupDirSrc.listFiles());
            zipOutStream.close();
            inStream.close();
        }  
        catch (IOException e) 
        {
            e.printStackTrace();
        }    
        
        //Compression process has been completed successfully
        Date endTime = new Date();
        out.println("The file compression process has completed successfully at " + (new Date()).toString());
    }
    
    /**
     * Compresses all files in a directory recursively.
     * It uses zip compression.
     * @param files Takes a list of files from a starting directory
     */
    private static void compressDirectory(File [] files) 
    {
        for ( File file : files ) 
        {
            try 
            {
                if ( file.isDirectory() ) 
                {
                    compressDirectory(file.listFiles());
                } 
                else if ( file.isFile() ) 
                {
                    //Setup the paths for the correct input and output streams
                    String path = file.getPath();
                    String relativePath = path.substring( path.indexOf(worldName), path.length()-1 );
                    zipOutStream.putNextEntry(new ZipEntry( relativePath ));
                    inStream = new FileInputStream(path);
                    
                    //Copy the input data into a compressed zip output format
                    byte[] buffer = new byte[1024];
                    int len;
                    while((len=inStream.read(buffer)) != -1)
                    {
                        zipOutStream.write(buffer, 0, len);
                    }
                }
            }
            catch (IOException e) 
            {
                e.printStackTrace();
            } 
        }
    }
    
    /**
     * Cleans up excess zip files stored on the local disk.
     * Ideally, none would be left on the disk.
    */
    private static void localStorageCleanup()
    {
        //Fetch local files in the backup directory
        File[] files = new File(backupDirDest.toString()).listFiles();
        
        if ( files.length > localFileStoreageThreshold ) {
            //Iterate through all of the files in the backup directory
            //This will return a list sorted in the opposite direction
            //TODO: Understand why this is true.
            for ( int i = files.length-1; i >= 0 ; i--) 
            {
                File file = files[i];
                if ( i > (files.length - localFileStoreageThreshold - 1 ) ) 
                {
                   file.delete();
                   out.println("Deleted Local File: " + file.getName());
                }
            }
        }
    }
    
    /*=================================================================
     ||                      Dropbox Methods                         ||
    ===================================================================*/
    /**
     * Initializes a temporary drop box connection with the default access credentials. 
     * @return A drop box client connection 
     */
    private static DbxClient initializeDropboxConnection()
    {
        DbxRequestConfig config = new DbxRequestConfig(
            "BackupScript/0.1", Locale.getDefault().toString());
        return new DbxClient(config, Access_Token);
    }
    
    /**
     * Writes the zipped files to the given dropbox application.
     */
    private static void copyToDropbox() 
    {
        //Initialize dropbox connection using keys
        DbxClient dropboxConnection = initializeDropboxConnection();
        
        //Print the date the dropbox upload starts at.
        out.println("Started writing the compressed file to dropbox at " + (new Date()).toString()) ;
        
        try 
        {
            File inputFile = new File(zipDest);
            inStream = new FileInputStream( inputFile );
            dropboxConnection.uploadFile("/Recent/" + backupFileName, DbxWriteMode.add(), 
                                   inputFile.length(), inStream);
        } 
        catch (DbxException e) 
        {
            e.printStackTrace();
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        } 
        
        //Print the date dropbox upload finishes at.
        out.println("Finished writing the compressed file to dropbox at " + (new Date()).toString());
    }
       
    /*
    * Process:
    * 1.) Scan through the short term storage folder in the evening
    * 2.) Copy the most recent entry into the long term storage folder
    * 3.) Delete the rest of the entries so that only twelve remain.
    * 4.) Scan through the long term storage folder in the evening
    * 5.) Delete entries older than x days
    */
    /**
     * Removes short term backups by deleting any that exceed
     * the recent file storage threshold. The most recent backup
     * will be archived in the history backups directory. The
     * history backups directory will then delete older records
     * that exceed the history file storage threshold.
     */
    private static void dropboxCleanup() 
    {
        //Establish a dropbox connection for this operation
        DbxClient dropboxConnection = initializeDropboxConnection();
        
        //Print the date the dropbox upload starts at.
        out.println("Started cleaning up the dropbox datastore at " + (new Date()).toString()) ;
       
        try {
            //Fetch all children in the recent directory
            DbxEntry.WithChildren recentFiles = dropboxConnection.getMetadataWithChildren("/Recent");
            Object [] recentEntries = recentFiles.children.toArray();
            
            //If we are beyond the recent files threshold delete older files 
            if ( recentEntries.length > recentFileStorageThreshold ) 
            {
                //Iterate through all of the recent entries
                for (int i = recentEntries.length-1; i >= 0; i--)
                {
                    DbxEntry entry = (DbxEntry)recentEntries[i];
                    out.println("RecentEntry[" + i + "]: " + entry.name);

                    //If it is the most recent entry copy to the historical files
                    if (i == recentEntries.length-1)
                    {
                        dropboxConnection.copy(entry.path, "/History/" + entry.name);
                        out.println("Archived: " + entry.name);
                    }
                    //If it is older than the threshold remove it
                    else if ( i <= (recentEntries.length - recentFileStorageThreshold - 1) )
                    {
                        dropboxConnection.delete(entry.path);
                        out.println("Deleted: " + entry.name);
                    }
                }
            }
            
            //Fetch all children in the history directory
            DbxEntry.WithChildren historicalFiles = dropboxConnection.getMetadataWithChildren("/History");
            Object [] historicalEntries = historicalFiles.children.toArray();
            
            //If we are beyond our historical files threshold delete older files
            if ( historicalEntries.length > historicalFileStorageThreshold ) 
            {
                //Iterate through all of the historical entries
                for (int i = historicalEntries.length - 1 ; i >= 0; i-- ) 
                {
                    DbxEntry entry = (DbxEntry)historicalEntries[i];
                    out.println("HistoricalEntry[" + i + "]: " + entry.name);

                    if ( i <= (historicalEntries.length - historicalFileStorageThreshold - 1) ) 
                    {
                        dropboxConnection.delete(entry.path);
                        out.println("Deleted: " + entry.name);
                    }
                }
            }
                
        } catch (DbxException ex) {
            Logger.getLogger(Backup.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Print the date the dropbox upload starts at.
        out.println("Finished cleaning up the dropbox datastore at " + (new Date()).toString()) ;     
    }
}
