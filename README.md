# README #
**Version** :  0.1

This program is a java based backup script designed for a Linux server that uses screens to run Minecraft instances. However, it could be used as a template to create different types of backup programs. It has a simple workflow: first it zips directories, then it copies them into dropbox. It will do this every hour until the related minecraft screen terminates. 

### How do I get set up? ###
**Setup** :

* Create a drop box account  
* Create a drop box application.  
* Generate an access token for your application token.  
* Replace string for the application token.
* Replace string for directory destination.  
* Replace string for directory source.  
* Change backupTimeInterval if you want the backups more or less frequent.  

**Usage** :  

* To use this correctly, all you should run this in a screen on Linux. The screen name for your related process or Minecraft Server should be MC_HubWorld.  
* Use the following code to execute this on Linux.  

```
#!linux

Java -jar Backup.jar
...or...
screen -S "backup" Java -jar Backup.jar
```


### Who do I talk to? ###
* Repo owner or admin